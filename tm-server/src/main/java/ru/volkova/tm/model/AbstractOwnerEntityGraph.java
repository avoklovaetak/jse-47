package ru.volkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractOwnerEntityGraph extends AbstractEntityGraph {

    @Nullable
    @ManyToOne
    protected UserGraph user;

    @Column
    @NotNull
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @Enumerated(EnumType.STRING)
    @NotNull
    private Status status = Status.NOT_STARTED;

    @Column(name = "date_start")
    @Nullable
    private Date dateStart;

    @Column(name = "date_end")
    @Nullable
    private Date dateFinish;

    @Column(name = "created")
    @Nullable
    private Date created = new Date();

}
