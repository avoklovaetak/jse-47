package ru.volkova.tm.api.service.dto;

import ru.volkova.tm.api.repository.IRepository;
import ru.volkova.tm.dto.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
