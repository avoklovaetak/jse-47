package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.ITaskEndpoint;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.dto.Task;
import ru.volkova.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint (@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @WebMethod
    public Task addTask(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "entity", partName = "entity") Task entity
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getTaskDTOService().insert(entity);
    }

    @WebMethod
    public void clearTasks(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getTaskService().clear(userId);
    }

    @NotNull
    @WebMethod
    public List<Task> findAllTasks(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        return serviceLocator.getTaskDTOService().findAll(userId);
    }

    @Nullable
    @WebMethod
    public Task findTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        return serviceLocator.getTaskDTOService().findById(userId,id);
    }

    @WebMethod
    public void removeTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getTaskService().removeById(userId, id);
    }

    @WebMethod
    public void changeTaskStatusById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "id") Status status
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getTaskService().changeOneStatusById(userId, id, status);
    }

    @WebMethod
    public void changeTaskStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getTaskService()
                .changeOneStatusByName(userId, name, status);
    }

    @Nullable
    @WebMethod
    public Task findTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        return serviceLocator.getTaskDTOService()
                .findOneByIndex(userId, index);
    }

    @Nullable
    @WebMethod
    public Task findTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        return serviceLocator.getTaskDTOService().findOneByName(userId, name);
    }

    @WebMethod
    public void removeTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getTaskService().removeOneByName(userId, name);
    }

    @WebMethod
    public void updateTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getTaskService()
                .updateOneById(userId, id, name, description);
    }

    @WebMethod
    public void addTaskByUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getTaskService().add(userId, name, description);
    }

}
