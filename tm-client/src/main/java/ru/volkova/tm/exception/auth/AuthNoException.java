package ru.volkova.tm.exception.auth;

import ru.volkova.tm.exception.AbstractException;

public class AuthNoException extends AbstractException {

    public AuthNoException() {
        super("Error! No authorized user...");
    }

}
