package ru.volkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TaskGraph extends AbstractOwnerEntityGraph {

    @Nullable
    @ManyToOne
    private ProjectGraph project;

}
