package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.endpoint.IProjectTaskEndpoint;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.dto.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void bindTaskByProjectId(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId", partName = "taskId") String taskId
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getProjectTaskService().bindTaskByProjectId(userId, projectId, taskId);
    }

    @NotNull
    @WebMethod
    public List<Task> findAllTasksByProjectId(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        return serviceLocator.getProjectTaskDTOService().findAllTasksByProjectId(userId, projectId);
    }

    @WebMethod
    public void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getProjectTaskService().removeProjectById(userId, id);
    }

    @WebMethod
    public void unbindTaskByProjectId(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId,
            @NotNull @WebParam(name = "taskId", partName = "taskId") String taskId
    ) {
        serviceLocator.getSessionDTOService().validate(session);
        final String userId = session.getUser().getId();
        serviceLocator.getProjectTaskService().unbindTaskByProjectId(userId, projectId, taskId);
    }

}
