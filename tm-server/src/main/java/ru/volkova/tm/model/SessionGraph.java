package ru.volkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "session")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SessionGraph extends AbstractEntityGraph implements Cloneable {

    @Column
    @Nullable
    Long timestamp = System.currentTimeMillis();

    @Column
    @Nullable
    String signature;

    @NotNull
    @ManyToOne
    private UserGraph user;

    @Override
    public SessionGraph clone() {
        try {
            return (SessionGraph) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
