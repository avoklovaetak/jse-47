package ru.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.model.TaskGraph;

import java.util.List;

public interface ITaskServiceGraph extends IServiceGraph<TaskGraph> {

    TaskGraph insert(@NotNull TaskGraph taskGraph);

    void add(
            @NotNull String userId,
            @Nullable String name,
            @Nullable String description
    );

    void addAll(@Nullable List<TaskGraph> entities);

    void clear(@NotNull String userId);

    List<TaskGraph> findAll(@NotNull String userId);

    @Nullable
    TaskGraph findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    TaskGraph findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskGraph findOneByName(@NotNull String userId, @NotNull String name);

    void changeOneStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @Nullable final Status status
    );

    void changeOneStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final Status status
    );

    void removeById(@NotNull String userId,@NotNull String id);

    void removeOneByName(@NotNull String userId,@Nullable String name);

    void updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
