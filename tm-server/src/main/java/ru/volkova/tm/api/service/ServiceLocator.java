package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.dto.*;
import ru.volkova.tm.api.service.model.*;

public interface ServiceLocator {

    @NotNull
    IProjectServiceGraph getProjectService();

    @NotNull
    IProjectTaskServiceGraph getProjectTaskService();

    @NotNull
    ITaskServiceGraph getTaskService();

    @NotNull
    IUserServiceGraph getUserService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISessionServiceGraph getSessionService();

    @NotNull
    IBackupService getBackupService();

    @NotNull
    IAdminUserServiceGraph getAdminUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IAdminService getAdminService();

    @NotNull
    IConnectionService getConnectionService();

    @NotNull
    IAdminUserService getAdminUserDTOService();

    @NotNull
    IProjectService getProjectDTOService();

    @NotNull
    IProjectTaskService getProjectTaskDTOService();

    @NotNull
    ISessionService getSessionDTOService();

    @NotNull
    ITaskService getTaskDTOService();

    @NotNull
    IUserService getUserDTOService();

}
