package ru.volkova.tm.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.*;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.api.service.dto.*;
import ru.volkova.tm.api.service.model.*;
import ru.volkova.tm.component.Backup;
import ru.volkova.tm.endpoint.*;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.model.UserGraph;
import ru.volkova.tm.service.*;
import ru.volkova.tm.service.dto.*;
import ru.volkova.tm.service.model.*;
import ru.volkova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskServiceGraph taskServiceGraph = new TaskServiceGraph(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectServiceGraph projectServiceGraph = new ProjectServiceGraph(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IProjectTaskServiceGraph projectTaskServiceGraph = new ProjectTaskServiceGraph(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final IUserServiceGraph userServiceGraph = new UserServiceGraph(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService);

    @NotNull
    private final AdminUserServiceGraph adminUserServiceGraph = new AdminUserServiceGraph(connectionService, propertyService);

    @NotNull
    private final IAdminUserService adminUserService = new AdminUserService(connectionService, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(adminUserService, propertyService);

    @NotNull
    private final IAdminService adminService = new AdminService(this);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final BackupService backupService = new BackupService(this);

    @NotNull
    private final SessionServiceGraph sessionServiceGraph = new SessionServiceGraph(this, connectionService);

    @NotNull
    private final ISessionService sessionService = new SessionService(this, connectionService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    @NotNull
    private final AdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @SneakyThrows
    private void initPID() {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    @NotNull
    @Override
    public IProjectServiceGraph getProjectService() {
        return projectServiceGraph;
    }

    @NotNull
    @Override
    public IProjectTaskServiceGraph getProjectTaskService() {
        return projectTaskServiceGraph;
    }

    @NotNull
    @Override
    public ITaskServiceGraph getTaskService() {
        return taskServiceGraph;
    }

    @NotNull
    @Override
    public IUserServiceGraph getUserService() {
        return userServiceGraph;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public IAdminService getAdminService() {
        return adminService;
    }

    @NotNull
    @Override
    public IBackupService getBackupService() {
        return backupService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public ISessionServiceGraph getSessionService() {
        return sessionServiceGraph;
    }

    @NotNull
    @Override
    public IAdminUserServiceGraph getAdminUserService() {
        return adminUserServiceGraph;
    }

    @NotNull
    @Override
    public IConnectionService getConnectionService() {
        return connectionService;
    }

    @Override
    public @NotNull IAdminUserService getAdminUserDTOService() {
        return adminUserService;
    }

    @Override
    public @NotNull IProjectService getProjectDTOService() {
        return projectService;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskDTOService() {
        return projectTaskService;
    }

    @Override
    public @NotNull ISessionService getSessionDTOService() {
        return sessionService;
    }

    @Override
    public @NotNull ITaskService getTaskDTOService() {
        return taskService;
    }

    @Override
    public @NotNull IUserService getUserDTOService() {
        return userService;
    }

    private void initEndpoint(){
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(projectTaskEndpoint);
        registry(adminUserEndpoint);
        registry(adminEndpoint);
        registry(sessionEndpoint);
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl,endpoint);
    }

    public void init() {
       initPID();
       initEndpoint();
       initUser();
    }

    private void initUser() {
        //if (adminUserService.findByLogin("test") != null) {
            UserGraph user = adminUserServiceGraph.createUser("test", "test");
        //}
        //if (adminUserService.findByLogin("test") != null) {
            adminUserServiceGraph.createUserWithRole("test1", "test1", Role.ADMIN);
        //}
            //adminUserServiceGraph.updateUser(user.getId(), "Kate", "Volkova", "Sergeevna");
    }

}
